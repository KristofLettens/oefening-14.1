﻿
namespace Oefening14_1_MeerDimentionaleArrays
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWaarden = new System.Windows.Forms.TextBox();
            this.txtGetal = new System.Windows.Forms.TextBox();
            this.lbResultaat = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aantal waarden";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Een getal";
            // 
            // txtWaarden
            // 
            this.txtWaarden.Location = new System.Drawing.Point(22, 56);
            this.txtWaarden.Name = "txtWaarden";
            this.txtWaarden.Size = new System.Drawing.Size(242, 27);
            this.txtWaarden.TabIndex = 2;
            // 
            // txtGetal
            // 
            this.txtGetal.Location = new System.Drawing.Point(22, 130);
            this.txtGetal.Name = "txtGetal";
            this.txtGetal.Size = new System.Drawing.Size(242, 27);
            this.txtGetal.TabIndex = 3;
            // 
            // lbResultaat
            // 
            this.lbResultaat.FormattingEnabled = true;
            this.lbResultaat.ItemHeight = 20;
            this.lbResultaat.Location = new System.Drawing.Point(289, 31);
            this.lbResultaat.Name = "lbResultaat";
            this.lbResultaat.Size = new System.Drawing.Size(450, 324);
            this.lbResultaat.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 32);
            this.button1.TabIndex = 4;
            this.button1.Text = "Berekenen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(141, 200);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 32);
            this.button2.TabIndex = 5;
            this.button2.Text = "Sluiten";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 382);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbResultaat);
            this.Controls.Add(this.txtGetal);
            this.Controls.Add(this.txtWaarden);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWaarden;
        private System.Windows.Forms.TextBox txtGetal;
        private System.Windows.Forms.ListBox lbResultaat;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

