﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oefening14_1_MeerDimentionaleArrays
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int aantalWaarden = 0;
            int eenGetal = 0;
            if (!int.TryParse(txtWaarden.Text, out aantalWaarden) || !int.TryParse(txtGetal.Text, out eenGetal) || aantalWaarden > 10)
            {
                MessageBox.Show("Uw ingave is fout.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int[,] resultaat = new int[10, 10];
                string dezeLijn = "";

                lbResultaat.Items.Clear();

                for (int i = 0; i < 10; i++)
                {
                    if (i < aantalWaarden)
                    {
                        dezeLijn += eenGetal.ToString().PadLeft(8);
                    }
                    else
                    {
                        dezeLijn += 0.ToString().PadLeft(8);
                    }
                }

                for (int i = 0; i < 10; i++)
                {
                    lbResultaat.Items.Add(dezeLijn);
                }

            }
        }
    }
}
